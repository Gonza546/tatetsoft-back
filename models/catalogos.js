'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class catalogos extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      catalogos.belongsTo(models.empleados,{
        as:'empleados',
        foreignKey:'responsable'
      });
      catalogos.belongsTo(models.subastas,{
        as:'subastas',
        foreignKey:'subasta'
      });
    }
  };
  catalogos.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    descripcion: {
      allowNull:false,
      type:DataTypes.STRING
    },
    subasta: {
      allowNull:true,
      type:DataTypes.INTEGER
    },
    responsable: {
      allowNull:false,
      type:DataTypes.INTEGER
    }
  }, {
    sequelize,
    modelName: 'catalogos',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return catalogos;
};