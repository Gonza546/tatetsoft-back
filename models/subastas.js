'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class subastas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      subastas.belongsTo(models.personas,{
        as:'subastadores',
        foreignKey:'subastador'
      });
    }
  };
  subastas.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    fecha: {
      type:DataTypes.DATE
    },
    hora: {
      allowNull:false,
      type:DataTypes.TIME
    },
    estado: {
      type:DataTypes.STRING
    },
    subastador: {
      allowNull:true,
      type:DataTypes.INTEGER
    },
    ubicacion: {  
      allowNull:true,
      type:DataTypes.STRING
    },
    capacidadAsistentes: {
      allowNull:true,
      type:DataTypes.INTEGER
    },
    tieneDeposito: {
      type:DataTypes.STRING
    },
    seguridadPropia: {
      type:DataTypes.STRING
    },
    categoria: {
      type:DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'subastas',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return subastas;
};