'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class subastadores extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      

    }
  };
  subastadores.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    matricula: {
      type:DataTypes.STRING
    },
    region: {
      type:DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'subastadores',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return subastadores;
};