'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class paises extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };paises.init({
    numero: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    nombre: {
      allowNull: false,
      type:DataTypes.STRING
    },
    nombreCorto: {
      allowNull:true,
      type:DataTypes.STRING
    },
    capital: {
      allowNull:false,
      type:DataTypes.STRING},
    nacionalidad: {
      allowNull:false,
      type:DataTypes.STRING
    },
    idiomas: {
      allowNull:false,
      type:DataTypes.STRING}
  }, {
    sequelize,
    modelName: 'paises',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return paises;
};