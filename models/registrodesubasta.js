'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class registroDeSubasta extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  registroDeSubasta.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    subasta: {
      allowNull:false,
      type:DataTypes.INTEGER
    },
    duenio: {
      allowNull:false,
      type:DataTypes.INTEGER
    },
    producto: {
      allowNull:false,
      type:DataTypes.INTEGER
    },
    cliente: {
      allowNull:false,
      type:DataTypes.INTEGER
    },
    importe: {
      allowNull:false,
      type:DataTypes.DECIMAL
    },
    comision: {
      allowNull:false,
      type:DataTypes.DECIMAL
    }
  }, {
    sequelize,
    modelName: 'registroDeSubasta',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return registroDeSubasta;
};