'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class personas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  personas.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    documento: {
      //allowNull: false,
     // defaultValue:"0123",
      type: DataTypes.STRING
    },
    nombre: {
      //allowNull: false,
      type: DataTypes.STRING
    },
    direccion: {
      //defaultValue:"hduaishd",
      type: DataTypes.STRING
    },
    estado: {
      defaultValue:"activo",
      type: DataTypes.STRING
    },
    foto: {
      defaultValue:1234,
      type: DataTypes.INTEGER
    }
  }, {
    sequelize,
    modelName: 'personas',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return personas;
};