'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class productos extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      productos.belongsTo(models.empleados,{
        as:'empleados',
        foreignKey:'revisor'
      });
      productos.belongsTo(models.duenios,{
        as:'duenios',
        foreignKey:'duenio'
      });
    }
  };
  productos.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    fecha: {
      type:DataTypes.DATE
    },
    disponible: {
      type:DataTypes.STRING
    },
    descripcionCatalogo: {
      allowNull:true,
      defaultValue:"No Posee",
      type:DataTypes.STRING
    },
    descripcionCompleta: {
      allowNull:false,
      type:DataTypes.STRING
    },
    revisor: {
      allowNull:false,
      type:DataTypes.INTEGER
    },
    duenio: {
      allowNull:false,
      type:DataTypes.INTEGER
    }
  }, {
    sequelize,
    modelName: 'productos',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return productos;
};