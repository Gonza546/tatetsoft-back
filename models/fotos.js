'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class fotos extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  fotos.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    producto: {
      allowNull:false,
      type:DataTypes.INTEGER
    },
    foto: {
      allowNull:false,
      type:DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'fotos',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return fotos;
};