'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class duenios extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      duenios.belongsTo(models.personas,{
        as:'personas',
        foreignKey:'identificador'
      });
      duenios.belongsTo(models.empleados,{
        as:'empleados',
        foreignKey:'verificador'
      });
    }
  };
  duenios.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    numeroPais: {
      type:DataTypes.INTEGER
    },
    verificacionFinanciera: {
      type:DataTypes.STRING
    },
    verificacionJudicial: {
      type:DataTypes.STRING
    },
    calificacionRiesgo: {
      type:DataTypes.INTEGER
    },
    verificador: {
      allowNull:false,
      type:DataTypes.INTEGER
    }
  }, {
    sequelize,
    modelName: 'duenios',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return duenios;
};