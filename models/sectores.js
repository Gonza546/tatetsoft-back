'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class sectores extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  sectores.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    nombreSector: {
      allowNull:false,
      type:DataTypes.STRING
    },
    codigoSector: {
      allowNull:true,
      type:DataTypes.STRING
    },
    responsableSector: {
      allowNull:true,
      type:DataTypes.INTEGER}
  }, {
    sequelize,
    modelName: 'sectores',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return sectores;
};