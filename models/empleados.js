'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class empleados extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  empleados.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    cargo: {
      type:DataTypes.STRING
    },
    sector: {
      allowNull:true,
      type:DataTypes.INTEGER
    }
  }, {
    sequelize,
    modelName: 'empleados',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return empleados;
};