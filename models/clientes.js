'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class clientes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  clientes.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    numeroPais: {
      type:DataTypes.INTEGER
    },
    admitido: {
      type:DataTypes.STRING
    },
    categoria: {
      type:DataTypes.STRING
    },
    verificador: {
      allowNull:false,
      type:DataTypes.INTEGER
    }
  }, {
    sequelize,
    modelName: 'clientes',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return clientes;
};