'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class itemscatalogo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      itemscatalogo.belongsTo(models.catalogos,{
        as:'catalogos',
        foreignKey:'catalogo'
      });
      itemscatalogo.belongsTo(models.productos,{
        as:'productos',
        foreignKey:'producto'
      });
    }
  };
  itemscatalogo.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    catalogo: {
      allowNull:false,
      type:DataTypes.INTEGER
    },
    producto: {
      allowNull:false,
      type:DataTypes.INTEGER
    },
    precioBase: {
      allowNull:false,
      type:DataTypes.DECIMAL
    },
    comision: {
      allowNull:false,
      type:DataTypes.DECIMAL
    },
    subastado: {
      type:DataTypes.STRING
    }
  }, {
    sequelize,
    freezeTableName:true,
    modelName: 'itemscatalogo',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return itemscatalogo;
};