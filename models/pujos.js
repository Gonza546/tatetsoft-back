'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class pujos extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      pujos.belongsTo(models.asistentes,{
        as:'asistentes',
        foreignKey:'asistente'
      });
    }
  };
  pujos.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    asistente: {
    allowNull:false,
    type:DataTypes.INTEGER
  },
    item: {
      allowNull:false,
      type:DataTypes.INTEGER
    },
    importe: {
      allowNull:false,
      type:DataTypes.DECIMAL
    },
    ganador: {
      defaultValue:"no",
      type:DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'pujos',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return pujos;
};