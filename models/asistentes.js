'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class asistentes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      asistentes.belongsTo(models.personas,{
        as:'personas',
        foreignKey:'cliente'
      });
      asistentes.belongsTo(models.subastas,{
        as:'subastas',
        foreignKey:'subasta'
      });
    }
  };
  asistentes.init({
    identificador: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    numeroPostor: {
      allowNull:false,
      type:DataTypes.INTEGER
    },
    cliente: {
      allowNull:false,
      type:DataTypes.INTEGER
    },
    subasta: {
      allowNull:false,
      type:DataTypes.INTEGER
    }
  }, {
    sequelize,
    modelName: 'asistentes',
    timestamps: false,
		createdAt: false,
		updatedAt: false
  });
  return asistentes;
};