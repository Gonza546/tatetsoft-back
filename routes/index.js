// Controllers
const usuariosController = require('../controllers/usuarios');
const juegosController = require('../controllers/juegos');
const participationController = require('../controllers/participation');
const personasController = require('../controllers/personas');
const subastasController = require('../controllers/subastas');
const catalogosController = require('../controllers/catalogos');
const itemsCatalogoController = require('../controllers/itemsCatalogo');
const asistentesController= require ('../controllers/asistentes');
const pujosController=require('../controllers/pujos');

module.exports = (app) => {

	app.get('/ad', (req, res) => res.status(200).send({
		message: 'Faltan hacer las fk en todas las tablas.',
	}));

	// Routes of Web Services

	app.post('/aw/personas/create',personasController.create);
	app.get('/aw/personas/list',personasController.list);
	app.get('/aw/subastas/:estado', subastasController.buscarSubastas);
	app.get('/aw/subastas/id/:subastaId',catalogosController.buscarCatalogoPorId);
	app.get('/aw/subastas/catalogo/:catalogoId',itemsCatalogoController.buscarItemsCatalogo);
	app.post('/aw/asistentes/create',asistentesController.create);
	app.post('/aw/pujas/create',pujosController.create);
	app.get('/aw/pujas/:item',pujosController.listarPujasPorId);


};