const Sequelize = require('sequelize');
const { Op } = require("sequelize");
const subastas = require('../models').subastas;
const personas = require('../models').personas;

module.exports = {


	buscarSubastas(req, res) {

		var fechaInicial = new Date();
		//fechaInicial=fechaInicial.getFullYear()+'-'+fechaInicial.getMonth()+'-'+fechaInicial.getDate();
    	//const fechaFinal = new Date(fechaInicial.getFullYear(), fechaInicial.getMonth(), fechaInicial.getDate());
		//fechaFinal.setHours(0,0,0,0);
		//const horaInicial = fechaInicial.getTime();
		//console.log("Fecha Final:",fechaFinal);
		//console.log("Fecha Inicial:",fechaInicial.toISOString().split('T'));
		//var prueba = fechaFinal.toISOString().split('T')[0];
		//console.log("asdkjaskldsa",prueba);
		//var prueba2 = new Date(prueba+'T00:00:00Z');
		//console.log("jiuriourioq",prueba2);
		if(req.params.estado=='abierta'){
			return subastas
			.findAll({	

				include:[
					{model:personas,
					as:'subastadores'
				}
				],
				where: {
					estado: req.params.estado
					
				}
			})
			.then(subastas => res.status(200).send(subastas))
			.catch(error => res.status(400).send(error))
		}else{
			return subastas
			.findAll({
				
				where: {
					estado: req.params.estado,
					fecha:{
						[Op.gt]:fechaInicial,
						
					}
				}
			})
			.then(subastas => res.status(200).send(subastas))
			.catch(error => res.status(400).send(error))
		}

	}

}