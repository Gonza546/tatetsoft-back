const Sequelize = require('sequelize');
const pujos = require('../models').pujos;
const asistentes=require('../models').asistentes
const personas=require('../models').personas;

module.exports = {


	create(req, res) {
		
		return pujos
			.create({     
				asistente:req.body.asistente,
                importe:req.body.importe,
                item:req.body.item
			})
			.then(pujos => res.status(200).send({status:200,message:"La persona se registro correctamente.",pujos}))
			.catch(error => res.status(400).send(error))
	},
    listarPujasPorId(req,res){
        return pujos
        .findAll({
            include:[
                {model:asistentes,
                as:'asistentes',
                include:[{model:personas,
                    as:'personas',
                    where:{identificador:asistentes.cliente}}]}
            ],
            where:{
                item:req.params.item
            },
            order: [
                ['identificador', 'DESC']
            ]

        })
        .then(pujos => res.status(200).send({status:200,message:"La persona se registro correctamente.",pujos}))
        .catch(error => {res.status(400).send(error);console.log(error);})
    }


}