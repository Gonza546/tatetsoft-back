const Sequelize = require('sequelize');
const catalogos = require('../models').catalogos;
const subastas = require('../models').subastas;

module.exports = {
    buscarCatalogoPorId(req, res) {
        return catalogos.findAll({
            include:[
                {model:subastas,
                as:'subastas'
            }
            ],
            where:{
                subasta:req.params.subastaId
            }
        })
        .then(catalogos => res.status(200).send(catalogos))
        .catch(error => {res.status(400).send(error);
        console.log(error);})
        
    }
}