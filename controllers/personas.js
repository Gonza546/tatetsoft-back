const Sequelize = require('sequelize');
const personas = require('../models').personas;
var PersonasService = require('../service/personas.js');

module.exports = {


	create(req, res) {
		
		return personas
			.findOrCreate({     //CAMBIAR EL CREATE POR FINDORCREATE. VER CLASE 1:47 muestra el find or create
				where:{
					documento:req.body.documento,
				},
				nombre: req.body.nombre,
				documento: req.body.documento,
				direccion: req.body.direccion,
				estado:'incativo', // en la base estaba asi el script
				foto:req.body.foto
			})
			.then(persona => res.status(200).send({status:200,message:"La persona se registro correctamente.",persona}))
			.catch(error => res.status(400).send(error))
	},

    list(_, res) {
		return personas
			.findAll({})
			.then(personas => res.status(200).send(personas))
			.catch(error => res.status(400).send(error))
	}

}