const Sequelize = require('sequelize');
const asistentes = require('../models').asistentes;

module.exports = {


	create(req, res) {
		console.log(req);
		return asistentes
			.findOrCreate({     
				where:{
					cliente:req.body.cliente,
                    numeroPostor:req.body.numeroPostor,
                    subasta:req.body.subasta
				},
				numeroPostor:req.body.numeroPostor,
                subasta:req.body.subasta,
                cliente:req.body.cliente
			})
			.then(asistentes => res.status(200).send({status:200,message:"La persona se registro correctamente.",asistentes}))
			.catch(error => res.status(400).send(error))
	}
}