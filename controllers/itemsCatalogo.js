const Sequelize = require('sequelize');
const itemscatalogo = require('../models').itemscatalogo;
const productos = require('../models').productos;

module.exports = {
    buscarItemsCatalogo(req, res) {
        return itemscatalogo.findAll({
            
            include:[
                {model:productos,
                as:'productos'
            }
            ],
            where:{
                catalogo:req.params.catalogoId
            }
        })
        .then(itemscatalogo => res.status(200).send(itemscatalogo))
        .catch(error => {res.status(400).send(error);})
    }
}